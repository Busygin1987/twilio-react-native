import 'react-native-gesture-handler';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'react-native-elements';
import { NavigationContainer } from '@react-navigation/native';

import store from './store/store';
import WebSocketProvider from './webSocket';
import Navigator from './navigation-v5/index';

const App = () => (
  <NavigationContainer>
    <ThemeProvider>
      <Provider store={store}>
        <WebSocketProvider>
          <View style={styles.container}>
            <Navigator />
          </View>
        </WebSocketProvider>
      </Provider>
    </ThemeProvider>
  </NavigationContainer>
);

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
