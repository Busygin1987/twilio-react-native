import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import { BASE_URI, PORT } from '../config';

export default async ({ method, url, data = {}, extraHeaders = {} }) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
    ...extraHeaders,
  };

  const token = await AsyncStorage.getItem('access_token');
  if (token) {
    headers.Authorization = `Bearer ${token}`;
  }

  const ax = axios.create({
    baseURL: `${BASE_URI}:${PORT}/`,
    headers,
  });

  if (method === 'get' && Object.keys(data).length > 0) {
    const params = Object.entries(data)
      .map((param) => `${param[0]}=${param[1]}`)
      .join('&');
    url += `?${params}`;
  }

  return ax[method](url, data);
};
