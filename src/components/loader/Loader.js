import React from 'react';
import { ActivityIndicator, StyleSheet, View, Dimensions } from 'react-native';

const { height, width } = Dimensions.get('window');

export default function App() {
  return (
    <View style={[styles.container, styles.horizontal]}>
      <ActivityIndicator size="large" color="#f4511e" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
    minHeight: height * 0.9,
    width: width,
  },
});
