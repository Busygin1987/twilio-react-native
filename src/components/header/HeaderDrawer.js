import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useDispatch } from 'react-redux';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import { logout } from '../../store/actions/auth';
import { clearUserStore, clearUserError } from '../../store/actions/user';

const Header = ({ navigation, title }) => {
  const dispatch = useDispatch();

  const openMenu = () => {
    navigation.openDrawer();
    dispatch(clearUserError());
  };

  const hadleLogout = () => {
    dispatch(logout());
    dispatch(clearUserStore());
    navigation.navigate('Auth');
  };

  return (
    <View style={styles.header}>
      <MaterialIcon
        name="menu"
        size={28}
        onPress={openMenu}
        style={styles.burger}
      />
      <View>
        <Text style={styles.headerText}>{title}</Text>
      </View>
      <MaterialIcon
        name="exit-to-app"
        size={28}
        onPress={hadleLogout}
        style={styles.logout}
      />
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#333',
    letterSpacing: 1,
    fontFamily: 'IndieFlower',
  },
  burger: {
    position: 'absolute',
    left: 16,
  },
  logout: {
    position: 'absolute',
    right: 16,
  },
});
