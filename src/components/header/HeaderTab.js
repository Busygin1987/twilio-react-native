import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useDispatch } from 'react-redux';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import { logout } from '../../store/actions/auth';
import { clearUserStore } from '../../store/actions/user';
import { appStyles } from '../../AppStyles';

const Header = ({ title, leftButton, navigation, isShowbackButton }) => {
  const dispatch = useDispatch();

  const hadleLogout = () => {
    dispatch(clearUserStore());
    dispatch(logout());
  };

  const handleBack = () => {
    navigation.goBack();
  };

  return (
    <View style={styles.header}>
      {isShowbackButton && (
        <MaterialIcon
          name="arrow-back"
          size={28}
          onPress={handleBack}
          style={styles.leftButton}
        />
      )}
      <View>
        <Text style={styles.headerText}>{title}</Text>
      </View>
      <MaterialIcon
        name="exit-to-app"
        size={28}
        onPress={hadleLogout}
        style={styles.logout}
      />
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  header: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 60,
    backgroundColor: appStyles.primaryColor,
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#333',
    letterSpacing: 1,
    fontFamily: 'IndieFlower',
  },
  burger: {
    position: 'absolute',
    left: 16,
  },
  logout: {
    position: 'absolute',
    right: 16,
  },
  leftButton: {
    position: 'absolute',
    left: 16,
  },
});
