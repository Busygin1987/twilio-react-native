import React from 'react';
import { TextInput, StyleSheet, View, Text } from 'react-native';

const MainInput = ({
  placeholder,
  handleChange,
  secure,
  value,
  style = {},
  label,
}) => {
  return (
    <View>
      <Text style={styles.label}>{label}</Text>
      <TextInput
        style={[styles.input, style]}
        onChangeText={(text) => handleChange(text)}
        placeholder={placeholder}
        secureTextEntry={secure}
        value={value}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    borderRadius: 5,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#969693',
    alignSelf: 'stretch',
    paddingLeft: 10,
    height: 40,
    marginBottom: 10,
    marginTop: 5,
  },
  label: {
    paddingLeft: 10,
    fontWeight: 'bold',
    fontSize: 14,
  },
});

export default MainInput;
