import React from 'react';
import { TextInput, StyleSheet, View } from 'react-native';

const ChatInput = ({ placeholder, handleChange, value, style = {} }) => {
  return (
    <View>
      <TextInput
        style={[styles.input, style]}
        onChangeText={text => handleChange(text)}
        placeholder={placeholder}
        value={value}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#969693',
    paddingLeft: 10,
    height: 50,
    fontSize: 18,
  },
});

export default ChatInput;