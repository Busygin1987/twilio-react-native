import React from 'react';
import { TouchableOpacity, StyleSheet, Text, View } from 'react-native';

import { appStyles } from '../../AppStyles';

const ChatButton = ({ style = {}, onPress, text }) => (
  <TouchableOpacity onPress={onPress} style={[styles.button, style]}>
    <View style={styles.block}>
      <Text>{text}</Text>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  button: {
    display: 'flex',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: appStyles.primaryColor,
    shadowColor: appStyles.primaryColor,
    shadowOpacity: 0.4,
    shadowOffset: { height: 10, width: 0 },
    shadowRadius: 20,
    width: 'auto',
  },
  text: {
    fontSize: 16,
    textTransform: 'uppercase',
    color: '#FFFFFF',
  },
  block: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
});

export default ChatButton;
