import React from 'react';
import { TouchableOpacity, StyleSheet, Text, View } from 'react-native';

import { appStyles } from '../../AppStyles';

const CustomButton = (props) => {
  const {
    title = null,
    style = {},
    textStyle = {},
    onPress,
    icon = null,
  } = props;

  return (
    <TouchableOpacity onPress={onPress} style={[styles.button, style]}>
      <View style={styles.block}>
        {icon && icon}
        {title && <Text style={[styles.text, textStyle]}>{title}</Text>}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    display: 'flex',
    height: 50,
    // borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: appStyles.primaryColor,
    shadowColor: appStyles.primaryColor,
    shadowOpacity: 0.4,
    shadowOffset: { height: 10, width: 0 },
    shadowRadius: 20,
  },
  text: {
    fontSize: 16,
    textTransform: 'uppercase',
    color: '#FFFFFF',
  },
  block: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
});

export default CustomButton;
