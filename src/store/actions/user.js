import api from '../../services/api';
import AsyncStorage from '@react-native-community/async-storage';

import * as types from './types';

const fetchUser = () => ({ type: types.FETCH_USER_DATA_REQUEST });
const successFetch = (payload) => ({
  type: types.FETCH_USER_DATA_SUCCESS,
  payload,
});
const failureFetch = (payload) => ({
  type: types.FETCH_USER_DATA_FAILURE,
  payload,
});

export const fetchUserDataAction = () => async (dispatch) => {
  dispatch(fetchUser());
  try {
    const response = await api({ method: 'get', url: '/user' });
    if (response.status === 200 && response.data.user) {
      dispatch(successFetch(response.data.user));
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureFetch(error.response.data.message));
    } else {
      dispatch(failureFetch(error));
    }
  }
};

const fetchUsers = () => ({ type: types.FETCH_USERS_REQUEST });
const successFetchUsers = (payload) => ({
  type: types.FETCH_USERS_SUCCESS,
  payload,
});
const failureFetchUsers = (payload) => ({
  type: types.FETCH_USERS_FAILURE,
  payload,
});

export const fetchUsersDataAction = () => async (dispatch) => {
  dispatch(fetchUsers());
  try {
    const response = await api({ method: 'get', url: '/user/all' });
    if (response.status === 200 && response.data.users) {
      dispatch(successFetchUsers(response.data.users));
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureFetchUsers(error.response.data.message));
    } else {
      dispatch(failureFetchUsers(error));
    }
  }
};

const fetchFriends = () => ({ type: types.FETCH_FRIENDS_REQUEST });
const successFetchFriends = (payload) => ({
  type: types.FETCH_FRIENDS_SUCCESS,
  payload,
});
const failureFetchFriends = (payload) => ({
  type: types.FETCH_FRIENDS_FAILURE,
  payload,
});

export const fetchUserFriendsAction = () => async (dispatch) => {
  dispatch(fetchFriends());
  try {
    const response = await api({ method: 'get', url: '/user/friends' });
    if (response.status === 200 && response.data.friends) {
      const friends = response.data.friends.map((friend) => ({
        ...friend,
        connected: false,
      }));
      dispatch(successFetchFriends(friends));
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureFetchFriends(error.response.data.message));
    } else {
      dispatch(failureFetchFriends(error));
    }
  }
};

const addFriend = () => ({ type: types.CREATE_NEW_FRIEND_REQUEST });
const successAddFriend = (payload) => ({
  type: types.CREATE_NEW_FRIEND_SUCCESS,
  payload,
});
const failureAddFriend = (payload) => ({
  type: types.CREATE_NEW_FRIEND_FAILURE,
  payload,
});

export const addNewFriendAction = (friend) => async (dispatch) => {
  dispatch(addFriend());
  try {
    const response = await api({
      method: 'post',
      url: '/user/friend',
      data: { friendId: friend.id },
    });
    if (response.status === 200) {
      dispatch(
        successAddFriend({
          ...friend,
          roomId: response.data.roomId,
          connected: false,
        }),
      );
      return response.data.roomId;
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureAddFriend(error.response.data.message));
    } else {
      dispatch(failureAddFriend(error));
    }
  }
};

const deleteFriend = () => ({ type: types.DELETE_FRIEND_REQUEST });
const successDeleteFriend = (payload) => ({
  type: types.DELETE_FRIEND_SUCCESS,
  payload,
});
const failureDeleteFriend = (payload) => ({
  type: types.DELETE_FRIEND_FAILURE,
  payload,
});

export const deleteFriendAction = (friend) => async (dispatch) => {
  dispatch(deleteFriend());
  try {
    const response = await api({
      method: 'delete',
      url: `/user/friend/${friend.id}`,
    });
    if (response.status === 204) {
      dispatch(successDeleteFriend(friend));
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureDeleteFriend(error.response.data.message));
    } else {
      dispatch(failureDeleteFriend(error));
    }
  }
};

const createUser = () => ({ type: types.CREATE_USER_REQUEST });
const successCreate = (payload) => ({
  type: types.CREATE_USER_SUCCESS,
  payload,
});
const failureCreate = (payload) => ({
  type: types.CREATE_USER_FAILURE,
  payload,
});

export const createUserAction = (userData) => async (dispatch) => {
  dispatch(createUser());
  try {
    const { status, data } = await api({
      method: 'post',
      url: '/user',
      data: { user: userData },
    });
    if (status === 200 && data.token) {
      const token = data.token.split(' ')[1];
      await AsyncStorage.setItem('access_token', token);
      dispatch(successCreate(data.user));
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureCreate(error.response.data.message));
    } else {
      dispatch(failureCreate(error));
    }
  }
};

export const clearUserStore = () => async (dispatch) => {
  dispatch({ type: types.CLEAR_USER_STORE });
};

export const loginByFacebook = (data) => ({
  type: types.SIGN_IN_BY_FACEBOOK,
  payload: data,
});

export const clearUserError = () => ({ type: types.CLEAR_USER_ERROR });

const updateUser = () => ({ type: types.UPDATE_USER_REQUEST });
const successUpdate = (payload) => ({
  type: types.UPDATE_USER_SUCCESS,
  payload,
});
const failureUpdate = (payload) => ({
  type: types.UPDATE_USER_FAILURE,
  payload,
});

export const updateUserDataAction = (userData) => async (dispatch) => {
  dispatch(updateUser());
  try {
    const { status, data } = await api({
      method: 'put',
      url: '/user',
      data: userData,
      extraHeaders: {
        'Content-Type': 'multipart/form-data',
        Accept: 'application/json',
      },
    });

    if (status === 200 && data.user) {
      dispatch(successUpdate(data.user));
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureUpdate(error.response.data.message));
    } else {
      dispatch(failureUpdate(error));
    }
  }
};
