import api from '../../services/api';
import jwtDecode from 'jwt-decode';
import AsyncStorage from '@react-native-community/async-storage';

import * as types from './types';

const requestSignUp = () => ({ type: types.SIGN_UP_REQUEST });
const successSignUp = (payload) => ({ type: types.SIGN_UP_SUCCESS, payload });
const failureSignUp = (payload) => ({ type: types.SIGN_UP_FAILURE, payload });

export const signUpAction = (userData) => async (dispatch) => {
  dispatch(requestSignUp());
  try {
    const response = await api({
      method: 'post',
      url: '/auth/sign-up',
      data: userData,
    });
    if (response.status === 200 && response.data.token) {
      const token = response.data.token.split(' ')[1];
      const decoded = jwtDecode(token);
      await AsyncStorage.setItem('access_token', token);
      dispatch(successSignUp(decoded));
      return true;
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureSignUp(error.response.data.message));
    } else {
      dispatch(failureSignUp(error));
    }
    return false;
  }
};

const requestSignIn = () => ({ type: types.SIGN_IN_REQUEST });
const successSignIn = (payload) => ({ type: types.SIGN_IN_SUCCESS, payload });
const failureSignIn = (payload) => ({ type: types.SIGN_IN_FAILURE, payload });

export const signInAction = (userData) => async (dispatch) => {
  dispatch(requestSignIn());
  try {
    const response = await api({
      method: 'post',
      url: '/auth/sign-in',
      data: userData,
    });
    if (response.status === 200 && response.data.token) {
      const token = response.data.token.split(' ')[1];
      const decoded = jwtDecode(token);
      await AsyncStorage.setItem('access_token', token);
      dispatch(successSignIn(decoded));
      return true;
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureSignIn(error.response.data.message));
    } else {
      dispatch(failureSignIn(error));
    }
    return false;
  }
};

const logoutUser = () => ({ type: types.LOGOUT_USER });

export const logout = () => async (dispatch) => {
  await AsyncStorage.removeItem('access_token');
  dispatch(logoutUser());
};

export const clearError = () => ({ type: types.CLEAR_AUTH_ERRROR });
