import api from '../../services/api';

import * as types from './types';

const getMessagesRequest = () => ({ type: types.JOIN_ROOM_REQUEST });
const getMessagesSuccess = (payload) => ({
  type: types.JOIN_ROOM_SUCCESS,
  payload,
});
const getMessagesError = (payload) => ({
  type: types.JOIN_ROOM_ERROR,
  payload,
});

export const getRoomMessages = ({ roomId, ...params }) => async (dispatch) => {
  dispatch(getMessagesRequest());
  try {
    const response = await api({
      method: 'get',
      url: `/ws/room/${roomId}`,
      params,
    });
    if (response.data.data.room) {
      dispatch(getMessagesSuccess(response.data.data));
    }
  } catch (error) {
    dispatch(getMessagesError(error));
  }
};

export const setUsername = (payload) => ({ type: types.SET_USERNAME, payload });
export const updateChatLog = (payload) => ({
  type: types.UPDATE_CHAT_LOG,
  payload,
});
export const setNewMessage = (payload) => ({
  type: types.SHOW_NEW_MESSAGE_BADGE,
  payload,
});

const twilioTokenRequest = () => ({ type: types.TWILIO_TOKEN_REQUEST });
const twilioTokenSuccess = (payload) => ({
  type: types.GET_TWILIO_TOKEN_SUCCESS,
  payload,
});
const twilioTokenError = (payload) => ({
  type: types.GET_TWILIO_TOKEN_ERROR,
  payload,
});

export const getTwilioToken = (roomId, identity) => async (dispatch) => {
  dispatch(twilioTokenRequest());
  try {
    const response = await api({
      method: 'get',
      url: `/twilio/token?room=${roomId}&identity=${identity}`,
    });
    if (response.data) {
      dispatch(twilioTokenSuccess(response.data));
      return response.data.token;
    }
  } catch (error) {
    dispatch(twilioTokenError(error));
  }
};

const twilioCreateRoomRequest = () => ({ type: types.TWILIO_CREATE_ROOM });
const twilioCreateRoomSuccess = (payload) => ({
  type: types.TWILIO_CREATE_ROOM_SUCCESS,
  payload,
});
const twilioCreateRoomError = (payload) => ({
  type: types.TWILIO_CREATE_ROOM_ERROR,
  payload,
});

export const createTwilioRoom = (name, type) => async (dispatch) => {
  dispatch(twilioCreateRoomRequest());
  try {
    const response = await api({
      method: 'post',
      url: '/twilio/room',
      data: { name, type },
    });
    if (response.data?.room) {
      dispatch(twilioCreateRoomSuccess(response.data.room));
    } else if (response.data.message) {
      dispatch(twilioCreateRoomError(response.data.message));
    }
  } catch (error) {
    dispatch(twilioCreateRoomError(error));
  }
};

const twilioUpdateRoomRequest = () => ({ type: types.TWILIO_DELETE_ROOM });
const twilioUpdateRoomSuccess = () => ({
  type: types.TWILIO_DELETE_ROOM_SUCCESS,
});
const twilioUpdateRoomError = (payload) => ({
  type: types.TWILIO_DELETE_ROOM_ERROR,
  payload,
});

export const deleteTwilioRoom = (roomSid) => async (dispatch) => {
  dispatch(twilioUpdateRoomRequest());
  try {
    const response = await api({
      method: 'put',
      url: `/twilio/room/${roomSid}?status=completed`,
    });
    if (response.status === 200) {
      dispatch(twilioUpdateRoomSuccess());
    }
  } catch (error) {
    dispatch(twilioUpdateRoomError(error));
  }
};

export const clearVideoChatError = () => ({ type: types.CLEAR_TWILIO_ERROR });
