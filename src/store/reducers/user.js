import * as types from '../actions/types';

const initialState = {
  user: {},
  users: [],
  friends: [],
  loading: false,
  error: null,
  wsConnected: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_USER_DATA_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.FETCH_USER_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        user: action.payload,
      };
    case types.FETCH_USER_DATA_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case types.FETCH_USERS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.FETCH_USERS_SUCCESS:
      return {
        ...state,
        loading: false,
        users: action.payload,
      };
    case types.FETCH_USERS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case types.FETCH_FRIENDS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.FETCH_FRIENDS_SUCCESS:
      return {
        ...state,
        loading: false,
        friends: action.payload,
      };
    case types.FETCH_FRIENDS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case types.CREATE_NEW_FRIEND_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.CREATE_NEW_FRIEND_SUCCESS:
      const user = state.friends.find(
        (friend) => friend.id === action.payload.id,
      );
      if (!user) {
        return {
          ...state,
          loading: false,
          friends: [...state.friends, action.payload],
        };
      } else {
        return {
          ...state,
          loading: false,
        };
      }

    case types.CREATE_NEW_FRIEND_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case types.DELETE_FRIEND_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.DELETE_FRIEND_SUCCESS:
      return {
        ...state,
        loading: false,
        friends: state.friends.filter((item) => item.id !== action.payload.id),
      };
    case types.DELETE_FRIEND_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case types.CREATE_USER_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.CREATE_USER_SUCCESS:
      return {
        ...state,
        user: action.payload,
        error: null,
        loading: false,
      };
    case types.CREATE_USER_FAILURE:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    case types.CLEAR_USER_STORE:
      return {
        ...state,
        user: {},
        friends: [],
        users: [],
      };
    case types.SIGN_IN_BY_FACEBOOK:
      return {
        ...state,
        user: action.payload,
      };
    case types.CLEAR_USER_ERROR:
      return {
        ...state,
        error: null,
      };
    case types.UPDATE_USER_SUCCESS:
      return {
        ...state,
        user: action.payload,
      };
    case types.UPDATE_USER_FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};
