import * as types from '../actions/types';

const initialState = {
  user: {},
  loading: false,
  error: null,
  isAuthenticated: false,
  logout: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.SIGN_UP_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.SIGN_UP_SUCCESS:
      return {
        ...state,
        loading: false,
        user: action.payload,
        isAuthenticated: true,
      };
    case types.SIGN_UP_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case types.SIGN_IN_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.SIGN_IN_SUCCESS:
      return {
        ...state,
        loading: false,
        user: action.payload,
        isAuthenticated: true,
      };
    case types.SIGN_IN_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case types.LOGOUT_USER:
      return {
        ...state,
        user: {},
        isAuthenticated: false,
        logout: true,
      };
    case types.CLEAR_AUTH_ERRROR:
      return {
        ...state,
        error: '',
      };
    default:
      return state;
  }
};
