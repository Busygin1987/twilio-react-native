import { createSelector } from 'reselect';

export const selectMessages = createSelector(
  state => state.chat.messages,
  messages => messages
);