import { createSelector } from 'reselect';

export const selectUserData = createSelector(
  (state) => state.me,
  (data) => data,
);

export const selectUsersData = createSelector(
  (state) => state.me.users,
  (state) => state.me.friends,
  (users, friends) =>
    users.filter((user) => friends.every((friend) => friend.id !== user.id)),
);

export const selectFriendsData = createSelector(
  (state) => state.me,
  (data) => data.friends,
);

export const selectUserName = createSelector(
  (state) => state.me,
  (data) => data.user.firstName,
);

export const selectUserId = createSelector(
  (state) => state.me,
  (data) => data.user.id,
);

export const selectUser = createSelector(
  (state) => state.me,
  (data) => data.user,
);
