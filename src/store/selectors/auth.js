import { createSelector } from 'reselect';

export const selectIsAuthenticated = createSelector(
  (state) => state.auth,
  (data) => data.isAuthenticated,
);
