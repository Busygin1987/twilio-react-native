import React from 'react';
import io from 'socket.io-client';
import _ from 'lodash';
import { useDispatch, useSelector } from 'react-redux';

import { updateChatLog, setNewMessage } from './store/actions/chat';
import { selectFriendsData, selectUser } from './store/selectors/user';

import { BASE_URI, PORT } from './config';

export const WebSocketContext = React.createContext(null);

export default function WSSocket({ children }) {
  const socketRef = React.useRef(null);
  let ws;

  const friends = useSelector(selectFriendsData);
  const user = useSelector(selectUser);
  const dispatch = useDispatch();

  const sendMessage = (payload) => {
    socketRef.current.emit('event://send-message', JSON.stringify(payload));

    const { body, room_id, from, to } = payload;
    dispatch(
      updateChatLog({
        userId: from,
        message: body,
        room_id,
        recipientId: to,
      }),
    );
  };

  React.useEffect(() => {
    friends.forEach((friend) => {
      socketRef.current.emit(
        'event://connect-to-room',
        JSON.stringify({
          room_id: friend.roomId,
          user_id: friend.id,
        }),
      );
    });
  }, [friends]);

  if (!socketRef.current) {
    socketRef.current = io.connect(`${BASE_URI}:${PORT}`, { forceNode: true });

    socketRef.current.on('event://success-connect', (msg) => {
      console.log('MSG ', msg);
    });

    socketRef.current.on('event://get-message', (msg) => {
      const payload = JSON.parse(msg);
      console.log(1111, payload);
      dispatch(updateChatLog(payload));
      if (user.id !== payload.userId) {
        dispatch(setNewMessage(payload));
      }
    });
  }

  ws = {
    socket: socketRef.current,
    sendMessage,
  };

  return (
    <WebSocketContext.Provider value={ws}>{children}</WebSocketContext.Provider>
  );
}
