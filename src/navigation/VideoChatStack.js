import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Dimensions } from 'react-native';

import VideoChatScreen from '../screens/videoChat/VideoChat';
import SetupChatScreen from '../screens/videoChat/SetupShat';
import Header from '../components/header/HeaderDrawer';

const { width } = Dimensions.get('window');

export default createStackNavigator(
  {
    VideoChat: {
      screen: VideoChatScreen,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: `Room name ${navigation.getParam('roomName')}`,
          headerTitleContainerStyle: {
            width: width,
          },
          headerTitleStyle: {
            fontFamily: 'IndieFlower',
          },
        };
      },
    },
    SetupVideoChat: {
      screen: SetupChatScreen,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: () => (
            <Header navigation={navigation} title={'Setup video chat'} />
          ),
          headerTitleContainerStyle: {
            width: width,
            left: 0,
          },
        };
      },
    },
  },
  {
    initialRouteName: 'SetupVideoChat',
    defaultNavigationOptions: {
      headerStyle: {
        height: 90,
        backgroundColor: '#eee',
      },
      headerTintColor: '#444',
    },
  },
);
