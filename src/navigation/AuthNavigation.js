import { createStackNavigator } from 'react-navigation-stack';

import SignIn from '../screens/signIn/SignIn';
import SignUp from '../screens/signUp/SignUp';
import Main from '../screens/main/Main';

export default createStackNavigator(
  {
    SignIn: {
      screen: SignIn,
      navigationOptions: {
        title: 'Sing In',
      },
    },
    SignUp: {
      screen: SignUp,
      navigationOptions: {
        title: 'Sing Up',
      },
    },
    Main: {
      screen: Main,
      navigationOptions: {
        title: 'Home',
      },
    },
  },
  {
    initialRouteName: 'Main',
    headerMode: 'none',
  },
);
