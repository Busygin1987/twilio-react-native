import { createDrawerNavigator } from 'react-navigation-drawer';
import { createAppContainer } from 'react-navigation';

import UserStack from './UserStack';
import ChatStack from './ChatStack';
import VideoChatStack from './VideoChatStack';

export default createAppContainer(
  createDrawerNavigator(
    {
      User: {
        screen: UserStack,
      },
      Chat: {
        screen: ChatStack,
        navigationOptions: {
          title: 'SMS Chat',
        },
      },
      VideoChat: {
        screen: VideoChatStack,
        navigationOptions: {
          title: 'Video Chat',
        },
      },
    },
    {
      intialRouteName: 'User',
      contentOptions: {
        activeTintColor: '#2196F3',
        activeBackgroundColor: '#BDD6F6',
        inactiveTintColor: '#878383',
        labelStyle: {
          fontSize: 18,
          fontFamily: 'IndieFlower',
        },
        itemsContainerStyle: {
          marginVertical: 10,
        },
        iconContainerStyle: {
          opacity: 1,
        },
      },
      drawerBackgroundColor: '#ECE9E8', // sets background color of drawer
    },
  ),
);
