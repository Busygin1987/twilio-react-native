import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Dimensions } from 'react-native';

import UserScreen from '../screens/user/User';
import Header from '../components/header/HeaderDrawer';

const width = Dimensions.get('window').width;

export default createStackNavigator(
  {
    UserScreen: {
      screen: UserScreen,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: () => (
            <Header navigation={navigation} title={'User Screen'} />
          ),
          headerTitleContainerStyle: {
            width: width,
            left: 0,
          },
        };
      },
    },
  },
  {
    initialRouteName: 'UserScreen',
    defaultNavigationOptions: {
      headerStyle: {
        height: 90,
        backgroundColor: '#eee',
      },
      headerTintColor: '#444',
    },
  },
);
