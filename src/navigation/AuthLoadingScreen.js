import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { View, Linking } from 'react-native';

import Loader from '../components/loader/Loader';

const AuthLoadingScreen = ({ navigation }) => {
  React.useEffect(() => {
    const checkToken = async () => {
      let userToken;

      try {
        userToken = await AsyncStorage.getItem('access_token');
        if (userToken) {
          navigation.navigate('App');
        } else {
          navigation.navigate('Auth');
        }
      } catch {
        console.log('Something went wrong!');
      }
    };

    checkToken();
  });

  return (
    <View>
      <Loader />
    </View>
  );
};

export default AuthLoadingScreen;
