import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import AuthStack from './AuthNavigation';
import AppStack from './AppNavigation';
import AuthLoadingScreen from './AuthLoadingScreen';

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    },
  ),
);
