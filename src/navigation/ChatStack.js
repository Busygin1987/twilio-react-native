import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Dimensions } from 'react-native';

import ChatScreen from '../screens/smsChat/ChatScreen';
import PrivateChatScreen from '../screens/smsChat/PrivateChatScreen';
import Header from '../components/header/HeaderDrawer';

const { width } = Dimensions.get('window');

export default createStackNavigator(
  {
    ChatScreen: {
      screen: ChatScreen,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: () => (
            <Header navigation={navigation} title={'Chat Screen'} />
          ),
          headerTitleContainerStyle: {
            width: width,
            left: 0,
          },
        };
      },
    },
    PrivateChat: {
      screen: PrivateChatScreen,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: `Private chat with ${navigation.getParam('firstName')}`,
          headerTitleContainerStyle: {
            width: width,
            // left: 0,
            // zIndex: -1,
            // alignItems: 'center'
          },
          headerTitleStyle: {
            fontFamily: 'IndieFlower',
          },
        };
      },
    },
  },
  {
    initialRouteName: 'ChatScreen',
    defaultNavigationOptions: {
      headerStyle: {
        height: 90,
        backgroundColor: '#eee',
      },
      headerTintColor: '#444',
    },
  },
);
