import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Tabs from './Tabs';
import Header from '../components/header/HeaderTab';
import PrivateChatScreen from '../screens/smsChat/PrivateChatScreen';
import VideoChatScreen from '../screens/videoChat/VideoChat';

const Stack = createStackNavigator();

const forFade = ({ current }) => ({
  cardStyle: {
    opacity: current.progress,
  },
});

export default () => (
  <Stack.Navigator
    headerMode="screen"
    screenOptions={{
      header: ({ scene, previous, navigation, ...rest }) => {
        const { options } = scene.descriptor;

        const title =
          options.headerTitle !== undefined
            ? options.headerTitle
            : options.title !== undefined
            ? options.title
            : scene.route.name;

        return (
          <Header
            title={title}
            isShowbackButton={previous}
            navigation={navigation}
          />
        );
      },
      cardStyleInterpolator: forFade,
    }}>
    <Stack.Screen
      name={'Main'}
      component={Tabs}
      options={{
        headerTitle: 'My App',
      }}
    />
    <Stack.Screen
      name="PrivateChat"
      component={PrivateChatScreen}
      options={{
        headerTitle: 'Private chat',
      }}
    />
    <Stack.Screen
      name="VideoChat"
      component={VideoChatScreen}
      options={({ route }) => ({ title: `Room name ${route.params.roomName}` })}
    />
  </Stack.Navigator>
);
