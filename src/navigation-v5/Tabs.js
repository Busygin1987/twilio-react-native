/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Badge } from 'react-native-elements';
import { View } from 'react-native';
import { useSelector } from 'react-redux';

import UserScreen from '../screens/user/User';
import ChatScreen from '../screens/smsChat/ChatScreen';
import SetupChatScreen from '../screens/videoChat/SetupShat';

import { selectSenredIds } from '../store/selectors/chat';
import { appStyles } from '../AppStyles';

const Tab = createBottomTabNavigator();

export default () => {
  const senderIds = useSelector(selectSenredIds);
  // console.log('senderIds ', senderIds);
  return (
    <Tab.Navigator
      intialRouteName="User"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color, size }) => {
          let iconName;
          if (route.name === 'User') {
            iconName = 'person';
          } else if (route.name === 'Chat') {
            iconName = 'chat';
          } else {
            iconName = 'videocam';
          }

          return (
            <View>
              {route.name === 'Chat' && senderIds.length > 0 && (
                <Badge
                  status="error"
                  containerStyle={{
                    position: 'absolute',
                    top: -4,
                    left: 26,
                  }}
                  badgeStyle={{
                    paddingBottom: 3,
                  }}
                  value="new"
                />
              )}
              <Icon name={iconName} size={size} color={color} />
            </View>
          );
        },
      })}
      tabBarOptions={{
        activeTintColor: appStyles.primaryColor,
        inactiveTintColor: 'gray',
      }}>
      <Tab.Screen
        name="User"
        component={UserScreen}
        options={{ tabBarLabel: 'User' }}
      />
      <Tab.Screen
        name="Chat"
        options={{
          tabBarLabel: 'Chat',
        }}
        component={ChatScreen}
      />
      <Tab.Screen
        name="SetupVideoChat"
        options={{ tabBarLabel: 'Video chat' }}
        component={SetupChatScreen}
      />
    </Tab.Navigator>
  );
};
