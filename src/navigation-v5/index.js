import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { useSelector } from 'react-redux';

import AppNavigation from './AppNavigation';
import AuthNavigation from './AuthNavigation';
import Loader from '../components/loader/Loader';

const MainNavigation = () => {
  const [isLogged, setLogged] = React.useState(false);
  const { isAuthenticated, logout } = useSelector((state) => state.auth);
  const [isChecking, setChecking] = React.useState(false);

  React.useEffect(() => {
    const checkToken = async () => {
      let userToken;

      setChecking(true);

      try {
        userToken = await AsyncStorage.getItem('access_token');

        if (!!userToken) {
          setLogged(true);
        } else {
          setLogged(false);
        }
      } catch {
        console.log('Something went wrong!');
      } finally {
        setChecking(false);
      }
    };

    checkToken();
  }, [isAuthenticated, logout]);

  if (isChecking) {
    return <Loader />;
  }

  return <>{isLogged ? <AppNavigation /> : <AuthNavigation />}</>;
};

export default MainNavigation;
