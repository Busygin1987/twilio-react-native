import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import SignIn from '../screens/signIn/SignIn';
import SignUp from '../screens/signUp/SignUp';
import Main from '../screens/main/Main';

const AuthStack = createStackNavigator();

export default () => (
  <AuthStack.Navigator initialRouteName="Home" headerMode="none">
    <AuthStack.Screen name={'SignIn'} component={SignIn} />
    <AuthStack.Screen name={'SignUp'} component={SignUp} />
    <AuthStack.Screen name={'Home'} component={Main} />
  </AuthStack.Navigator>
);
