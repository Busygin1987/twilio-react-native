import React from 'react';
import _ from 'lodash';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  Button,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Icon from 'react-native-vector-icons/Fontisto';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-picker';

import Loader from '../../components/loader/Loader';
import { selectUserData } from '../../store/selectors/user';
import {
  fetchUserDataAction,
  updateUserDataAction,
  fetchUserFriendsAction,
} from '../../store/actions/user';

import { createFormData } from '../../utils/helpers';
import { appStyles } from '../../AppStyles';
import { BASE_URI, PORT } from '../../config';

const { width, height } = Dimensions.get('window');

const UserScreen = () => {
  const dispatch = useDispatch();
  const { user, error } = useSelector(selectUserData);
  const { firstName, lastName, userName, avatar, email, id } = user;
  const [photo, setPhoto] = React.useState(null);
  const [token, setToken] = React.useState(null);
  const [initUpload, setInit] = React.useState(false);

  const checkExistUser = async () => {
    const userToken = await AsyncStorage.getItem('access_token');
    if (userToken) {
      setToken(userToken);
      return true;
    }

    return false;
  };

  React.useEffect(() => {
    if (_.has(user, 'firstName')) {
      dispatch(fetchUserFriendsAction());
    }

    const fetchUserData = async () => {
      const isAuthenticated = await checkExistUser();

      if (!_.has(user, 'firstName') && isAuthenticated) {
        dispatch(fetchUserDataAction());
      }
    };

    fetchUserData();
  }, [dispatch, user]);

  const handleChoosePhoto = async () => {
    const options = {
      noData: true,
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
        setInit(false);
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.uri) {
        if (avatar) {
          setInit(true);
        }
        setPhoto(response);
      }
    });
  };

  const handleUpload = async () => {
    setInit(false);
    const data = createFormData(photo, { id });
    dispatch(updateUserDataAction(data));
  };

  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>Your profile</Text>
      </View>
      {!_.has(user, 'firstName') ? (
        <Loader />
      ) : !error ? (
        <View style={styles.userDataBlock}>
          <View>
            {/* {console.log(`User ${user.firstName} - ${user.id}`)} */}
            <Text style={styles.userName}>
              User: {userName ? userName : `${firstName}  ${lastName}`}
            </Text>
            <Text style={styles.userEmail}>@: {email}</Text>
          </View>
          <View>
            {avatar && !initUpload ? (
              <>
                <Image
                  source={{
                    uri: `${BASE_URI}:${PORT}/photo/${avatar}`,
                    method: 'GET',
                    headers: {
                      Authorization: `Bearer ${token}`,
                    },
                  }}
                  style={{ width: 150, height: 150 }}
                />
                <View style={styles.button}>
                  <Button title="Change avatar" onPress={handleChoosePhoto} />
                </View>
              </>
            ) : (
              <View style={styles.pictureContainer}>
                {!photo && <Icon name="picture" size={50} color="#333" />}
                {photo && (
                  <React.Fragment>
                    <Image
                      source={{ uri: photo.uri }}
                      style={{ width: 150, height: 150 }}
                    />
                    <View style={styles.button}>
                      <Button title="Upload" onPress={handleUpload} />
                    </View>
                  </React.Fragment>
                )}
                <View style={styles.button}>
                  <Button title="Choose Photo" onPress={handleChoosePhoto} />
                </View>
              </View>
            )}
          </View>
        </View>
      ) : (
        <Text style={styles.error}>{error.toString()}</Text>
      )}
    </View>
  );
};

export default UserScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: appStyles.primaryBGcolor,
    flexDirection: 'column',
  },
  userName: {
    fontSize: 18,
    color: '#484848',
    fontFamily: 'IndieFlower',
  },
  userEmail: {
    fontSize: 14,
    color: '#484848',
    fontFamily: 'IndieFlower',
  },
  userDataBlock: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'column',
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: 20,
    height: height * 0.6,
    width: width * 0.7,
  },
  error: {
    marginVertical: 10,
    color: '#FF0000',
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'IndieFlower',
  },
  pictureContainer: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  button: {
    marginTop: 10,
  },
  titleContainer: {
    marginBottom: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
});
