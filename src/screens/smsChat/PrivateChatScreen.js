import React from 'react';
import { View, Text, StyleSheet, Dimensions, ScrollView } from 'react-native';
import { useSelector } from 'react-redux';
import { Avatar } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';

import { WebSocketContext } from '../../webSocket';
import ChatButton from '../../components/buttons/ChatButton';
import ChatInput from '../../components/inputs/ChatInput';
import Loader from '../../components/loader/Loader';

import { selectMessages, selectLoading } from '../../store/selectors/chat';
import { selectUser } from '../../store/selectors/user';

import { appStyles } from '../../AppStyles';

import { BASE_URI, PORT } from '../../config';

const { width } = Dimensions.get('window');

const PrivateChatScreen = ({ route }) => {
  const { id, roomId, firstName, lastName, avatar } = route.params;
  const ws = React.useContext(WebSocketContext);
  const [message, setMessage] = React.useState('');
  const [token, setToken] = React.useState(null);

  const messages = useSelector(selectMessages);
  const loading = useSelector(selectLoading);
  const user = useSelector(selectUser);

  React.useEffect(() => {
    setUserToken();
  }, []);

  const setUserToken = async () => {
    const userToken = await AsyncStorage.getItem('access_token');
    if (userToken) {
      setToken(userToken);
    }
  };

  const handleInput = (text) => setMessage(text);

  const sendMessage = () => {
    if (message) {
      ws.sendMessage({
        from: user.id,
        to: id,
        body: message,
        room_id: roomId,
      });
      setMessage('');
    }
  };

  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollBody}>
        {!loading ? (
          messages.length > 0 &&
          messages.map((item, index) => {
            const currentUser = item.userId === user.id;
            const termStyle = currentUser ? styles.user : styles.friend;
            const termDir = currentUser ? styles.userDir : styles.friendDir;
            const titleCharOne = currentUser ? user.firstName : firstName;
            const titleCharTwo = currentUser ? user.lastName : lastName;

            return (
              <View key={index} style={[styles.messageContainer, termDir]}>
                <Avatar
                  rounded
                  source={{
                    uri: `${BASE_URI}:${PORT}/photo/${
                      currentUser ? user.avatar : avatar
                    }`,
                    method: 'GET',
                    headers: {
                      Authorization: `Bearer ${token}`,
                    },
                  }}
                  title={`${titleCharOne
                    .toUpperCase()
                    .charAt()}${titleCharTwo.toUpperCase().charAt()}`}
                  containerStyle={{
                    backgroundColor: '#D3D3D3',
                    height: 40,
                    width: 40,
                  }}
                />
                <View
                  style={[termStyle, styles.messageBlock]}
                  key={Math.floor(Math.random() * 100)}>
                  <Text style={styles.text} numberOfLines={100}>
                    {item.message}
                  </Text>
                </View>
              </View>
            );
          })
        ) : (
          <Loader />
        )}
      </ScrollView>
      <View style={styles.enterData}>
        <ChatInput
          style={styles.input}
          value={message}
          handleChange={(text) => handleInput(text)}
          placeholder="Enter message"
        />
        <ChatButton text={'Send'} style={styles.button} onPress={sendMessage} />
      </View>
    </View>
  );
};

export default PrivateChatScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: appStyles.primaryBGcolor,
    borderWidth: 1,
    flexDirection: 'column',
  },
  text: {
    fontSize: 20,
    width: '100%',
    padding: 5,
    color: '#000',
    fontFamily: 'IndieFlower',
  },
  user: {
    backgroundColor: '#DCDCDC',
  },
  friend: {
    backgroundColor: '#6495ED',
  },
  enterData: {
    display: 'flex',
    flexDirection: 'row',
    height: 50,
    width: width,
  },
  scrollBody: {
    flexGrow: 1,
  },
  input: {
    width: width * 0.7,
  },
  button: {
    width: width * 0.3,
  },
  messageBlock: {
    width: width * 0.6,
    borderRadius: 5,
    marginHorizontal: 10,
    marginVertical: 5,
    padding: 5,
  },
  messageContainer: {
    width: width,
    display: 'flex',
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
  },
  userDir: {
    justifyContent: 'flex-start',
  },
  friendDir: {
    flexDirection: 'row-reverse',
  },
});
