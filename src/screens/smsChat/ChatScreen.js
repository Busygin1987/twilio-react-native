import React from 'react';
import { View, Text, StyleSheet, Dimensions, ScrollView } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import _ from 'lodash';

import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {
  fetchUsersDataAction,
  // fetchUserFriendsAction,
  addNewFriendAction,
  deleteFriendAction,
} from '../../store/actions/user';
import { selectUsersData, selectFriendsData } from '../../store/selectors/user';
import {
  selectSenredIds,
  // selectRecipientIds,
} from '../../store/selectors/chat';
// import { selectUser } from '../../store/selectors/user';
import { getRoomMessages } from '../../store/actions/chat';

import { appStyles } from '../../AppStyles';
import { PAGINATION } from '../../config';

const { width } = Dimensions.get('window');

const ChatScreen = ({ navigation: { navigate } }) => {
  const dispatch = useDispatch();

  const users = useSelector(selectUsersData);
  const friends = useSelector(selectFriendsData);
  const senderIds = useSelector(selectSenredIds);
  // const recipientIds = useSelector(selectRecipientIds);
  // const user = useSelector(selectUser);

  React.useEffect(() => {
    dispatch(fetchUsersDataAction());
  }, [dispatch]);

  const addNewFriend = async (friend) => {
    /*const roomId = await */ dispatch(
      _.debounce(addNewFriendAction(friend), 300),
    );
    // ws.socket.emit('event://connect-to-room', roomId);
  };

  const deleteFriend = (friend) =>
    dispatch(_.debounce(deleteFriendAction(friend), 300));

  const writeFriend = (friend) => {
    dispatch(
      getRoomMessages({
        roomId: friend.roomId,
        offset: PAGINATION.OFFSET,
        limit: PAGINATION.LIMIT,
      }),
    );
    // ws.socket.emit('event://connect-to-room', friend.roomId);
    navigate('PrivateChat', friend);
  };

  return (
    <View>
      <ScrollView contentContainerStyle={styles.container}>
        {friends.length === 0 && users.length === 0 && (
          <Text style={styles.notUsers}>Users not added yet!</Text>
        )}
        <Text style={styles.title}>Friends</Text>
        {friends.length > 0 && (
          <View style={styles.list}>
            {friends.map((friend) => {
              return (
                <View style={styles.userBlock} key={friend.id}>
                  <Text numberOfLines={1} style={styles.userBlockText}>
                    {friend.userName
                      ? friend.userName
                      : `${friend.firstName}  ${friend.lastName}`}
                  </Text>
                  <View style={styles.iconBlock}>
                    {/* {console.log('senderIds ', user.firstName, senderIds)} */}
                    {senderIds.includes(friend.id) && (
                      <Icon
                        style={{ marginRight: 10 }}
                        name="fiber-new"
                        size={22}
                        color="tomato"
                      />
                    )}
                    <FontAwesomeIcon
                      onPress={() => writeFriend(friend)}
                      style={{ marginRight: 10 }}
                      name="pencil-square-o"
                      size={22}
                      color="black"
                    />
                    <AntDesignIcon
                      onPress={() => deleteFriend(friend)}
                      name="deleteuser"
                      size={22}
                      color="black"
                    />
                  </View>
                </View>
              );
            })}
          </View>
        )}
        <Text style={styles.title}>All users</Text>
        {users.length > 0 && (
          <View style={styles.list}>
            {users.map((user) => {
              return (
                <View style={styles.userBlock} key={user.id}>
                  <Text numberOfLines={1} style={styles.userBlockText}>
                    {user.userName
                      ? user.userName
                      : `${user.firstName}  ${user.lastName}`}
                  </Text>
                  <IoniconsIcon
                    onPress={() => addNewFriend(user)}
                    name="md-person-add"
                    size={20}
                    color="black"
                  />
                </View>
              );
            })}
          </View>
        )}
      </ScrollView>
    </View>
  );
};

export default ChatScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: appStyles.primaryBGcolor,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 20,
    flexGrow: 1,
  },
  list: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    flexDirection: 'column',
    marginVertical: 10,
    width: width * 0.9,
    borderRadius: 10,
    padding: 10,
  },
  userBlock: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderColor: '#969693',
    borderWidth: 1,
    borderRadius: 5,
    width: width * 0.75,
    margin: 4,
    overflow: 'hidden',
    flexDirection: 'row',
    display: 'flex',
    justifyContent: 'space-between',
  },
  userBlockText: {
    fontSize: 18,
    color: '#000',
    fontFamily: 'IndieFlower',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 10,
    fontFamily: 'IndieFlower',
  },
  iconBlock: {
    display: 'flex',
    flexDirection: 'row',
  },
  notUsers: {
    paddingTop: 30,
    fontSize: 24,
    color: '#000',
  },
});
