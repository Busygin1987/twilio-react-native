import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { StyleSheet, View, ScrollView, Dimensions, Text } from 'react-native';

import CustomButton from '../../components/buttons/CustomButton';
import MainInput from '../../components/inputs/MainInput';

import { signUpAction, clearError } from '../../store/actions/auth';

import { validateEmail } from '../../utils/helpers';

import { appStyles } from '../../AppStyles';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const SignUp = ({ navigation }) => {
  const { error } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const [firstName, setFirstName] = React.useState('');
  const [lastName, setLastName] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [confirmPassword, setConformPassword] = React.useState('');
  const [validationError, setError] = React.useState('');

  React.useEffect(() => () => dispatch(clearError()), [dispatch]);

  const handleSubmit = async () => {
    if (
      firstName &&
      lastName &&
      validateEmail(email) &&
      password === confirmPassword
    ) {
      dispatch(signUpAction({ firstName, lastName, email, password }));
    } else {
      setError('You must fill in the fields with the correct data!');
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.title}>Sign up</Text>
      <View style={styles.formBlock}>
        <View>
          <MainInput
            handleChange={(text) => setFirstName(text)}
            placeholder={'Enter your first name'}
            value={firstName}
            label={'First Name'}
          />
          <MainInput
            handleChange={(text) => setLastName(text)}
            placeholder={'Enter your last name'}
            value={lastName}
            label={'Last Name'}
          />
          <MainInput
            handleChange={(text) => setEmail(text)}
            placeholder={'Enter your email'}
            value={email}
            label={'Email'}
          />
          <MainInput
            handleChange={(text) => setPassword(text)}
            placeholder={'Enter your password'}
            secure={true}
            value={password}
            label={'Password'}
          />
          <MainInput
            handleChange={(text) => setConformPassword(text)}
            placeholder={'Enter your password again'}
            secure={true}
            value={confirmPassword}
            label={'Confirn password'}
          />
        </View>
        {(!!validationError || !!error) && (
          <Text style={styles.error}>
            {validationError || error.toString()}
          </Text>
        )}
        <View style={styles.buttonBlock}>
          <CustomButton
            style={styles.button}
            title="Send"
            onPress={() => handleSubmit()}
          />
          <CustomButton
            style={styles.button}
            title="Go Back"
            onPress={() => navigation.goBack()}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: appStyles.primaryBGcolor,
    height: 'auto',
    paddingTop: 30,
  },
  button: {
    width: width / 2,
    borderRadius: 20,
    marginBottom: 10,
  },
  formBlock: {
    width: width * 0.8,
    minHeight: height * 0.6,
    elevation: 10,
    paddingVertical: 30,
    paddingHorizontal: 20,
    borderRadius: 5,
    marginBottom: 50,
    backgroundColor: '#fff',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 30,
    textAlign: 'center',
    margin: 10,
    height: 50,
    color: '#969693',
    fontFamily: 'IndieFlower',
  },
  buttonBlock: {
    marginTop: 20,
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  error: {
    marginVertical: 10,
    color: '#FF0000',
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'IndieFlower',
  },
});
