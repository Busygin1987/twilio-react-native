import React from 'react';
import { StyleSheet, View, ScrollView, Dimensions, Text } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import CustomButton from '../../components/buttons/CustomButton';
import MainInput from '../../components/inputs/MainInput';

import { signInAction, clearError } from '../../store/actions/auth';

import { validateEmail } from '../../utils/helpers';

import { appStyles } from '../../AppStyles';

const { width, height } = Dimensions.get('window');

const SignIn = ({ navigation }) => {
  const { error } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const [email, setEmail] = React.useState('artem@gmail.com');
  const [password, setPassword] = React.useState('123');
  const [validationError, setError] = React.useState('');

  React.useEffect(() => () => dispatch(clearError()), [dispatch]);

  const handleSubmit = async () => {
    if (validateEmail(email) && password) {
      dispatch(signInAction({ email, password }));
    } else {
      setError('You must fill in the fields with the correct data!');
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.title}>Sign in</Text>
      <View style={styles.formBlock}>
        <View>
          <MainInput
            handleChange={(text) => setEmail(text)}
            placeholder={'Enter your email'}
            value={email}
            label={'Email'}
          />
          <MainInput
            handleChange={(text) => setPassword(text)}
            placeholder={'Enter your password'}
            secure={true}
            value={password}
            label={'Password'}
          />
        </View>
        {(!!validationError || !!error) && (
          <View>
            <Text style={styles.error}>
              {validationError || error.toString()}
            </Text>
          </View>
        )}
        <View style={styles.buttonBlock}>
          <CustomButton
            style={styles.button}
            title="Send"
            onPress={() => handleSubmit()}
          />
          <CustomButton
            style={styles.button}
            title="Go back"
            onPress={() => navigation.goBack()}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default SignIn;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: appStyles.primaryBGcolor,
    height: '100%',
  },
  button: {
    width: width / 2,
    borderRadius: 20,
    marginBottom: 10,
  },
  formBlock: {
    width: width * 0.8,
    minHeight: height * 0.5,
    elevation: 10,
    padding: 15,
    borderRadius: 5,
    marginBottom: 50,
    backgroundColor: '#fff',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 30,
    textAlign: 'center',
    margin: 10,
    height: 50,
    color: '#969693',
    fontFamily: 'IndieFlower',
  },
  buttonBlock: {
    marginTop: 20,
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  error: {
    marginVertical: 10,
    color: '#FF0000',
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'IndieFlower',
  },
});
