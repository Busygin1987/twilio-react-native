import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Modal,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const MessageModal = ({
  modalState,
  setModalState,
  deleteMessage,
  updateMessage,
}) => {
  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalState}
        onRequestClose={() => setModalState(false)}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Icon
              style={styles.closeIcon}
              name="close"
              size={18}
              onPress={() => setModalState(false)}
            />
            <Text style={styles.modalText}>You can ...</Text>

            <TouchableHighlight
              style={styles.openButton}
              onPress={deleteMessage}>
              <Text style={styles.textStyle}>Delete message.</Text>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.openButton}
              onPress={() => updateMessage()}>
              <Text style={styles.textStyle}>Update message.</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default MessageModal;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  openButton: {
    borderRadius: 20,
    padding: 12,
    elevation: 2,
    marginVertical: 10,
    backgroundColor: '#2196F3',
  },
  closeIcon: {
    position: 'absolute',
    top: 15,
    right: 18,
  },
});
