import React from 'react';
import { StyleSheet, View } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { createLocalVideoTrack, connect } from 'twilio-video';
import HTML from 'react-native-render-html';

import { getTwilioToken } from '../../store/actions/chat';
import { selectUserName } from '../../store/selectors/user';
import { selectTwilioToken } from '../../store/selectors/chat';

const VideoComponent = ({ roomId }) => {
  const dispacth = useDispatch();

  const [videoContainer, setVideoContainer] = React.useState(null);

  const userName = useSelector(selectUserName);
  const token = useSelector(selectTwilioToken);

  React.useEffect(() => {
    dispacth(getTwilioToken(roomId, userName));
  }, []);

  React.useLayoutEffect(() => {
    if (token) {
      connect(token, {
        name: roomId,
        audio: true,
        video: true,
        tracks: [],
        _useTwilioConnection: true,
      })
        .then((room) => {
          console.log('DATA SUCCESS ', room);
          createLocalVideoTrack({
            audio: true,
            video: { width: 240, height: 200 },
          }).then((track) => {
            setVideoContainer(track.attach());
          });
          // room.on('participantConnected', function(participant) {
          //   console.log(participant.identity + ' has connected');
          // });
          // room.once('disconnected', function() {
          //   console.log('You left the Room:', room.name);
          // });
        })
        .catch((err) => console.log(err));
    }
  }, [token]);

  return (
    <View style={styles.container}>
      <HTML html={`${videoContainer}`} />
    </View>
  );
};

export default VideoComponent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%',
    width: '100%',
  },
});
