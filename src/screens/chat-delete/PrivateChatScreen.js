import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import { useSelector } from 'react-redux';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';

import { WebSocketContext } from '../../webSocket';
import ChatButton from '../../components/buttons/ChatButton';
import ChatInput from '../../components/inputs/ChatInput';
import VideoComponent from './VideoCompoent';
import Loader from '../../components/loader/Loader';

import { selectMessages, selectLoading } from '../../store/selectors/chat';
import { selectUserName, selectUserId } from '../../store/selectors/user';

import { appStyles } from '../../AppStyles';

const { width, height } = Dimensions.get('window');

const PrivateChatScreen = ({ navigation }) => {
  const ws = React.useContext(WebSocketContext);
  const [message, setMessage] = React.useState('');
  const [video, setVideo] = React.useState(false);

  const messages = useSelector(selectMessages);
  const loading = useSelector(selectLoading);
  const userName = useSelector(selectUserName);
  const userId = useSelector(selectUserId);

  const handleInput = (text) => setMessage(text);

  const sendMessage = () => {
    setMessage('');
    ws.sendMessage({
      from: userId,
      to: navigation.getParam('id'),
      body: message,
      room_id: navigation.getParam('roomId'),
    });
  };

  const setVideoChat = () => setVideo(!video);

  return (
    <View style={styles.container}>
      <View style={styles.nameBlockWrap}>
        <Text style={[styles.userSide, styles.nameBlock]}>YOU</Text>
        <TouchableOpacity onPress={() => setVideoChat()}></TouchableOpacity>
      </View>
      <ScrollView contentContainerStyle={styles.scrollBody}>
        {video && <VideoComponent roomId={navigation.getParam('roomId')} />}
        {!loading ? (
          messages.length > 0 &&
          messages.map((item, index) => {
            const termStyle =
              item.userId === userId ? styles.user : styles.friend;
            const termDir =
              item.userId === userId ? styles.userDir : styles.friendDir;
            return (
              <View key={index} style={[styles.messageContainer, termDir]}>
                <View
                  style={[termStyle, styles.messageBlock]}
                  key={Math.floor(Math.random() * 100)}>
                  <Text style={styles.text} numberOfLines={100}>
                    {item.message}
                  </Text>
                </View>
              </View>
            );
          })
        ) : (
          <Loader />
        )}
      </ScrollView>
      <View style={styles.enterData}>
        <ChatInput
          style={styles.input}
          value={message}
          handleChange={(text) => handleInput(text)}
          placeholder="Enter message"
        />
        <ChatButton style={styles.button} onPress={sendMessage} />
      </View>
    </View>
  );
};

export default PrivateChatScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: appStyles.primaryBGcolor,
    flexDirection: 'column',
  },
  text: {
    fontSize: 20,
    width: '100%',
    padding: 5,
    color: '#000',
    fontFamily: 'IndieFlower',
  },
  user: {
    backgroundColor: '#b6d7a8',
  },
  friend: {
    backgroundColor: '#ea9999',
  },
  nameBlockWrap: {
    display: 'flex',
    flexDirection: 'row',
    height: height * 0.06,
  },
  nameBlock: {
    width: width * 0.5,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    paddingTop: 5,
  },
  nameBlockWithCamera: {
    height: '100%',
    width: width * 0.5,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 5,
  },
  userSide: {
    backgroundColor: '#b6d7a8',
  },
  friendSide: {
    backgroundColor: '#ea9999',
  },
  enterData: {
    display: 'flex',
    flexDirection: 'row',
    height: height * 0.1,
    width: width,
  },
  scrollBody: {
    flexGrow: 1,
  },
  input: {
    width: width * 0.7,
  },
  button: {
    width: width * 0.3,
  },
  messageBlock: {
    width: width * 0.7,
    borderColor: '#333',
    borderRadius: 5,
    borderWidth: 1,
    marginHorizontal: 10,
    marginVertical: 5,
    padding: 5,
  },
  messageContainer: {
    width: width,
    display: 'flex',
    flexDirection: 'row',
  },
  userDir: {
    justifyContent: 'flex-start',
  },
  friendDir: {
    justifyContent: 'flex-end',
  },
});
