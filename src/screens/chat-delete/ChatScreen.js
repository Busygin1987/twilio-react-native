import React from 'react';
import { View, Text, StyleSheet, FlatList, Dimensions } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';

import {
  fetchUsersDataAction,
  fetchUserFriendsAction,
  addNewFriendAction,
  deleteFriendAction,
} from '../../store/actions/user';
import { selectUsersData, selectFriendsData } from '../../store/selectors/user';
import { getRoomMessages } from '../../store/actions/chat';

import { appStyles } from '../../AppStyles';

const { width, height } = Dimensions.get('window');

const ChatScreen = ({ navigation: { navigate } }) => {
  const dispatch = useDispatch();

  const users = useSelector(selectUsersData);
  const friends = useSelector(selectFriendsData);

  React.useEffect(() => {
    dispatch(fetchUsersDataAction());
    dispatch(fetchUserFriendsAction());
  }, []);

  const addNewFriend = async (friend) => {
    dispatch(addNewFriendAction(friend));
  };

  const deleteFriend = (friend) => dispatch(deleteFriendAction(friend));

  const writeFriend = (friend) => {
    dispatch(getRoomMessages(friend.roomId));
    navigate('PrivateChat', friend);
  };

  return (
    <View style={styles.container}>
      {friends.length === 0 && users.length === 0 && (
        <Text style={styles.notUsers}>Users not added yet!</Text>
      )}
      {friends.length > 0 && (
        <View style={styles.list}>
          <Text style={styles.title}>Friends</Text>
          <FlatList
            key={Math.floor(Math.random() * 1000)}
            keyExtractor={(item) => item.id}
            data={friends}
            renderItem={({ item }) => (
              <View style={styles.userBlock} key={item.id}>
                <Text numberOfLines={1} style={styles.userBlockText}>
                  {item.userName
                    ? item.userName
                    : `${item.firstName}  ${item.lastName}`}
                </Text>
                <View style={styles.iconBlock}>
                  <FontAwesomeIcon
                    onPress={() => writeFriend(item)}
                    style={{ marginRight: 10 }}
                    name="pencil-square-o"
                    size={22}
                    color="black"
                  />
                  <AntDesignIcon
                    onPress={() => deleteFriend(item)}
                    name="deleteuser"
                    size={22}
                    color="black"
                  />
                </View>
              </View>
            )}
          />
        </View>
      )}
      {users.length > 0 && (
        <View style={styles.list}>
          <Text style={styles.title}>All users</Text>
          <FlatList
            key={Math.floor(Math.random() * 1000)}
            keyExtractor={(item) => item.id}
            data={users}
            renderItem={({ item }) => (
              <View style={styles.userBlock} key={item.id}>
                <Text numberOfLines={1} style={styles.userBlockText}>
                  {item.userName
                    ? item.userName
                    : `${item.firstName}  ${item.lastName}`}
                </Text>
                <IoniconsIcon
                  onPress={() => addNewFriend(item)}
                  name="md-person-add"
                  size={20}
                  color="black"
                />
              </View>
            )}
          />
        </View>
      )}
    </View>
  );
};

export default ChatScreen;

const styles = StyleSheet.create({
  container: {
    height: height,
    backgroundColor: appStyles.primaryBGcolor,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 20,
    flexGrow: 1,
  },
  list: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    flexDirection: 'column',
    marginVertical: 10,
    width: width * 0.9,
    borderRadius: 10,
    padding: 10,
  },
  userBlock: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderColor: '#969693',
    borderWidth: 1,
    borderRadius: 5,
    width: width * 0.75,
    margin: 4,
    overflow: 'hidden',
    flexDirection: 'row',
    display: 'flex',
    justifyContent: 'space-between',
  },
  userBlockText: {
    fontSize: 18,
    color: '#000',
    fontFamily: 'IndieFlower',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 10,
    fontFamily: 'IndieFlower',
  },
  iconBlock: {
    display: 'flex',
    flexDirection: 'row',
  },
  notUsers: {
    paddingTop: 30,
    fontSize: 24,
    color: '#000',
  },
});
