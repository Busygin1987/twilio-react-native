import React from 'react';
import { View, Text, StyleSheet, Dimensions, Linking } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';
import { useDispatch } from 'react-redux';

import CustomButton from '../../components/buttons/CustomButton';
import { loginByFacebook } from '../../store/actions/user';
import { BASE_URI, PORT } from '../../config';

import { appStyles } from '../../AppStyles';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const Main = ({ navigation: { navigate } }) => {
  const dispatch = useDispatch();

  const handleOpenURL = React.useCallback(
    async ({ url }) => {
      const [, user_string] = url.match(/user=([^#]+)/);
      const [, token] = url.match(/token=([^&]+)/);

      await AsyncStorage.setItem('access_token', token);
      const user = JSON.parse(decodeURI(user_string));

      if (user.facebookId) {
        dispatch(loginByFacebook(user));
        navigate('App');
      }
    },
    [dispatch, navigate],
  );

  React.useEffect(() => {
    Linking.addEventListener('url', handleOpenURL);

    return () => {
      Linking.removeEventListener('url', handleOpenURL);
    };
  }, [handleOpenURL]);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Welcome!</Text>
      <View style={styles.buttonBlock}>
        <CustomButton
          style={styles.button}
          title="Sign In"
          onPress={() => navigate('SignIn')}
        />
        <CustomButton
          style={styles.button}
          title="Sign Up"
          onPress={() => navigate('SignUp')}
        />
        <CustomButton
          textStyle={{ paddingLeft: 8 }}
          style={styles.button}
          icon={<IoniconsIcon name="logo-facebook" size={24} color="#fff" />}
          title="Facebook"
          onPress={() => Linking.openURL(`${BASE_URI}:${PORT}/auth/facebook`)}
        />
      </View>
    </View>
  );
};

export default Main;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: appStyles.primaryBGcolor,
    borderWidth: 1,
    flexDirection: 'column',
  },
  title: {
    fontSize: 30,
    textAlign: 'center',
    margin: 10,
    height: 50,
    color: '#969693',
    fontFamily: 'IndieFlower',
  },
  buttonBlock: {
    width: width / 2,
    height: height / 6,
  },
  button: {
    marginTop: 10,
    fontSize: 20,
    borderRadius: 20,
    fontFamily: 'IndieFlower',
  },
});
