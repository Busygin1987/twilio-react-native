import React from 'react';
import { StyleSheet, View, Dimensions, Text } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { Picker } from '@react-native-community/picker';

import CustomButton from '../../components/buttons/CustomButton';
import MainInput from '../../components/inputs/MainInput';
import {
  getTwilioToken,
  createTwilioRoom,
  clearVideoChatError,
} from '../../store/actions/chat';
import { selectUserName } from '../../store/selectors/user';
import { selectTwilioRoom, selectChatError } from '../../store/selectors/chat';

import { appStyles } from '../../AppStyles';

const { width, height } = Dimensions.get('window');

const VideoComponent = ({ navigation }) => {
  const dispatch = useDispatch();

  const room = useSelector(selectTwilioRoom);
  const userName = useSelector(selectUserName);
  const error = useSelector(selectChatError);

  const [roomName, setRoomName] = React.useState('');
  const [roomType, setRoomType] = React.useState('group');
  const [message, setMessage] = React.useState('');

  React.useEffect(() => {
    room && setMessage('Room was created success!');
  }, [room]);

  const createRoom = () => {
    error && dispatch(clearVideoChatError());
    roomName && dispatch(createTwilioRoom(roomName, roomType));
  };

  const connectRoom = async () => {
    if (roomName) {
      const token = await dispatch(getTwilioToken(roomName, userName));
      dispatch(clearVideoChatError());
      setRoomName('');
      setMessage('');
      navigation.navigate('VideoChat', { roomName, token });
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Setup video chat</Text>
      <View style={styles.formBlock}>
        <Text style={{ paddingLeft: 10 }}>Select room type</Text>
        <Picker
          selectedValue={roomType}
          style={styles.picker}
          onValueChange={(itemValue) => setRoomType(itemValue)}>
          <Picker.Item label="P2P Room" value="peer-to-peer" />
          <Picker.Item label="Small Group Room" value="group-small" />
          <Picker.Item label="Group Room" value="group" />
        </Picker>
        <View>
          <MainInput
            handleChange={(text) => setRoomName(text)}
            placeholder={'Enter room name'}
            value={roomName}
            label={'Room name'}
          />
        </View>
        <View style={styles.buttonBlock}>
          <CustomButton
            style={styles.button}
            title="Create room"
            onPress={createRoom}
          />
          <CustomButton
            style={styles.button}
            title="Connect room"
            onPress={connectRoom}
          />
        </View>
        {!!message && !error && (
          <Text style={[styles.message, styles.success]}>{message}</Text>
        )}
        {!!error && <Text style={[styles.message, styles.error]}>{error}</Text>}
      </View>
    </View>
  );
};

export default VideoComponent;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: appStyles.primaryBGcolor,
    flexGrow: 1,
    paddingTop: 20,
  },
  formBlock: {
    width: width * 0.8,
    minHeight: height * 0.5,
    padding: 15,
    borderRadius: 10,
    marginBottom: 50,
    backgroundColor: '#fff',
    justifyContent: 'space-between',
  },
  setData: {
    display: 'flex',
    flexDirection: 'column',
  },
  buttonBlock: {
    marginTop: 20,
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  button: {
    width: width / 2,
    borderRadius: 50,
    marginBottom: 10,
  },
  picker: {
    borderRadius: 10,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#969693',
    alignSelf: 'stretch',
    paddingHorizontal: 10,
    height: 40,
    marginBottom: 10,
    marginTop: 5,
    fontSize: 14,
  },
  message: {
    fontSize: 18,
    marginVertical: 15,
    textAlign: 'center',
  },
  success: {
    color: '#228B22',
  },
  error: {
    color: '#CD0000',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 10,
    fontFamily: 'IndieFlower',
  },
});
