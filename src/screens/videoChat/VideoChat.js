/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {
  TwilioVideoLocalView,
  TwilioVideoParticipantView,
  TwilioVideo,
} from 'react-native-twilio-video-webrtc';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  PermissionsAndroid,
  Platform,
} from 'react-native';

export async function GetAllPermissions() {
  // it will ask the permission for user
  try {
    if (Platform.OS === 'android') {
      const userResponse = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      ]);
      return userResponse;
    }
  } catch (err) {
    console.log(err);
  }
  return null;
}

export default class VideoComponent extends Component {
  state = {
    isAudioEnabled: true,
    isVideoEnabled: true,
    status: 'disconnected',
    participants: new Map(),
    videoTracks: new Map(),
  };

  constructor(props) {
    super(props);
    this.twilioVideo = React.createRef();
  }

  componentDidMount() {
    // on start we are asking the permisions
    GetAllPermissions();
    const { roomName, token } = this.props.route.params;

    this.twilioVideo.current.connect({
      accessToken: token,
      roomName,
    });
  }

  componentWillUnmount() {
    this.twilioVideo.current.disconnect();
  }

  _onEndButtonPress = () => {
    this.twilioVideo.current.disconnect();
    this.props.navigation.goBack();
  };

  _onMuteButtonPress = () => {
    this.twilioVideo.current
      .setLocalAudioEnabled(!this.state.isAudioEnabled)
      .then((isEnabled) => this.setState({ isAudioEnabled: isEnabled }));
  };

  _onFlipButtonPress = () => {
    this.twilioVideo.current.flipCamera();
  };

  _onRoomDidConnect = ({ participants }) => {
    const namesOfPaticipants = participants
      .map((participant) => participant.identity)
      .join(', ');

    console.log('Room did connect: ', namesOfPaticipants);
    this.setState({ status: 'connected' });
  };

  _onRoomDidDisconnect = ({ roomName, error }) => {
    console.log('ERROR: ', roomName, error);

    this.setState({ status: 'disconnected' });
  };

  _onRoomDidFailToConnect = (error) => {
    console.log('ERROR: ', error);

    this.setState({ status: 'disconnected' });
  };

  _onParticipantAddedVideoTrack = ({ participant, track }) => {
    console.log(
      `Participant "${participant.identity}" ADDED video track: ${track.trackSid}`,
    );

    this.setState({
      videoTracks: new Map([
        ...this.state.videoTracks,
        [
          track.trackSid,
          { participantSid: participant.sid, videoTrackSid: track.trackSid },
        ],
      ]),
    });
  };

  _onParticipantRemovedVideoTrack = ({ participant, track }) => {
    console.log(
      `Participant "${participant.identity}" REMOVED video track: ${track.trackSid}`,
    );

    const videoTracks = this.state.videoTracks;
    videoTracks.delete(track.trackSid);

    this.setState({ videoTracks: { ...videoTracks } });
  };

  render() {
    return (
      <View style={styles.container}>
        {(this.state.status === 'connected' ||
          this.state.status === 'connecting') && (
          <View style={styles.callContainer}>
            {this.state.status === 'connected' && (
              <View style={styles.remoteGrid}>
                {Array.from(
                  this.state.videoTracks,
                  ([trackSid, trackIdentifier]) => {
                    if (trackSid) {
                      return (
                        <TwilioVideoParticipantView
                          style={styles.remoteVideo}
                          key={trackSid}
                          trackIdentifier={trackIdentifier}
                        />
                      );
                    } else {
                      return null;
                    }
                  },
                )}
              </View>
            )}
            <View style={styles.optionsContainer}>
              <TouchableOpacity
                style={styles.optionButton}
                onPress={this._onEndButtonPress}>
                <Text style={{ fontSize: 14, color: '#fff' }}>End</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.optionButton}
                onPress={this._onMuteButtonPress}>
                <Text style={{ fontSize: 14, color: '#fff' }}>
                  {this.state.isAudioEnabled ? 'Mute' : 'Unmute'}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.optionButton}
                onPress={this._onFlipButtonPress}>
                <Text style={{ fontSize: 14, color: '#fff' }}>Flip</Text>
              </TouchableOpacity>
              <TwilioVideoLocalView enabled={true} style={styles.localVideo} />
            </View>
          </View>
        )}

        <TwilioVideo
          ref={this.twilioVideo}
          onRoomDidConnect={this._onRoomDidConnect}
          onRoomDidDisconnect={this._onRoomDidDisconnect}
          onRoomDidFailToConnect={this._onRoomDidFailToConnect}
          onParticipantAddedVideoTrack={this._onParticipantAddedVideoTrack}
          onParticipantRemovedVideoTrack={this._onParticipantRemovedVideoTrack}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightgrey',
    flexDirection: 'row',
  },
  form: {
    flex: 1,
    alignSelf: 'center',
    backgroundColor: 'white',
    borderRadius: 10,
    margin: 20,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  formGroup: {
    margin: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button: {
    padding: 10,
    backgroundColor: 'blue',
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
  },
  textInput: {
    padding: 5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'lightgrey',
  },
  callContainer: {
    flex: 1,
  },
  callWrapper: {
    flex: 1,
    justifyContent: 'center',
  },
  remoteGrid: {
    flex: 1,
  },
  remoteVideo: {
    flex: 1,
    // width: width / 2,
    // height: height / 2,
  },
  localVideo: {
    position: 'absolute',
    right: 5,
    bottom: 50,
    flex: 1,
    width: 125,
    height: 150,
    borderRadius: 50,
    borderColor: '#4e4e4e',
  },
  optionsContainer: {
    position: 'absolute',
    paddingHorizontal: 10,
    left: 0,
    right: 0,
    bottom: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
