import React, { useRef, useState, useEffect } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import {
  createLocalVideoTrack,
  connect,
  createLocalTracks,
} from 'twilio-video';

import {
  selectTwilioToken,
  selectTwilioRoom,
} from '../../store/selectors/chat';
import { deleteTwilioRoom } from '../../store/actions/chat';

const { width, height } = Dimensions.get('window');

const VideoComponent = ({ navigation }) => {
  const dispatch = useDispatch();

  const token = useSelector(selectTwilioToken);
  const room = useSelector(selectTwilioRoom);
  const chatError = useSelector(selectChatError);
  const loading = useSelector(selectLoading);

  const localVideoRef = useRef(null);
  const remoteVideoRef = useRef(null);

  const [error, setError] = React.useState('');

  const addParticipant = (participant) => {
    console.log('Participant "%s" connected', participant.identity);

    const div = document.createElement('div');
    div.id = participant.sid;
    div.innerText = participant.identity;
    div.setAttribute(
      'style',
      'display: flex; flex-direction: column; align-items: center; font-weight: bold;',
    );

    participant.on('trackSubscribed', (track) => {
      div.appendChild(track.attach());
      remoteVideoRef.current.appendChild(div);
    });

    participant.tracks.forEach((publication) => {
      if (publication.isSubscribed) {
        const track = publication.track;
        div.appendChild(track.attach());
        remoteVideoRef.current.appendChild(div);
      }
    });
  };

  useEffect(() => {
    let twilioRoom = null;
    const setConnect = async () => {
      try {
        const localTracks = await createLocalTracks({
          audio: true,
          video: { width: 400 },
        });
    
        twilioRoom = await connect(token, {
          tracks: localTracks,
          name: room,
        });

        const [audio, video] = localTracks;
        localVideoRef.current.appendChild(video.attach());

        const track = await createLocalVideoTrack({
          audio: true,
          video: { width: 240, height: 240 },
        });

        twilioRoom.on('participantConnected', (participant) => {
          console.log(participant.identity + ' has connected');
        });

        twilioRoom.on('participantDisconnected', (participant) => {
          console.log(participant.identity + ' has disconnected');
        });

        twilioRoom.once('disconnected', async () => {
          if (room) {
            dispatch(deleteTwilioRoom(room.sid));
          }
          if (!room) {
            navigation.navigate('SetupVideoChat');
          }
          track.stop();
          console.log('You left the Room:', twilioRoom.name);
        });
      } catch (error) {
        setError(error.message);
      }
    };

    token && setConnect();

    return () => {
      if (twilioRoom) {
        twilioRoom.disconnect();
        twilioRoom = null;
      }
    };
  }, [dispatch, navigation, room, token]);

  return (
    <View style={styles.container}>
      <Text>HELLO FROM VIDEO CHAT</Text>
      {/* {!!error && <Text>{error}</Text>} */}
    </View>
  );
};

export default VideoComponent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    position: 'relative',
  },
});
